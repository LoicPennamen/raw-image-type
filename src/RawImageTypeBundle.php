<?php
namespace LoicPennamen\RawImageTypeBundle;

use \Symfony\Component\HttpKernel\Bundle\Bundle;

class RawImageTypeBundle extends Bundle
{
	public function getPath(): string
	{
		return \dirname(__DIR__);
	}
}
