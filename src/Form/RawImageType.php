<?php

namespace LoicPennamen\RawImageTypeBundle\Form;

use LoicPennamen\RawImageTypeBundle\Services\RawImageTypeService;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\DataMapperInterface;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RawImageType extends AbstractType implements DataMapperInterface
{
	private $kernel;

	public function __construct(KernelInterface $kernel)
	{
		$this->kernel = $kernel;
	}

	// public function
	public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('file', FileType::class, [
            	'required' => $options['required'],
            	'label' => false,
            	'mapped' => false,
				'attr' => [
					'accept' => $this->getAcceptStr($options),
				]
			])
            ->add('base64', TextType::class, [
            	'required' => false,
            	'label' => false,
            	'mapped' => false,
			])
			// configure the data mapper for this FormType
			->setDataMapper($this)
        ;
    }

    public function getAcceptStr(array $options)
	{
		return implode(', ', $options['allowed_extensions']);
	}

	public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
			// Allowed file extensions
            'allowed_extensions' => ['.jpg', '.jpeg', '.png', '.gif', '.bmp'],
			// Delete existing file when filename option is specified and no data was posted
            'delete_if_empty' => true,
			// Display additional informations
            'debug' => false,
			// Text to display if no image was submitted
            'empty_text' => 'No image',
			// Desired file name after upload. See "shortname". Set to NULL or 'auto' to auto-generate a unique name
            'filename' => null,
			// Maximum width and height of the optimized posted image
			'max_width' => 1920,
			'max_height' => 1080,
			// Maximum width of the preview window, as CSS string
			'preview_max_width' => '100%',
			// Preview ratio - can be an arithmetic string
            'ratio' => '16/9',
            // Delete button title
            'remove_text' => 'Delete this image',
        	// Required or not
            'required' => false,
        	// Desired file name after upload - without the extension.
            'shortname' => null,
			// Where the actual file is stored
            'upload_path' => 'public/uploads/',
        ]);
    }

	public function buildView(FormView $view, FormInterface $form, array $options)
	{
		$view->vars['ratio'] = $options['ratio'];
		$view->vars['empty_text'] = $options['empty_text'];
		$view->vars['remove_text'] = $options['remove_text'];
		$view->vars['debug'] = $options['debug'];
		$view->vars['max_width'] = $options['max_width'];
		$view->vars['max_height'] = $options['max_height'];
		$view->vars['preview_max_width'] = $options['preview_max_width'];

		parent::buildView($view, $form, $options);
	}

	/**
	 * @param string|null $imagePath
	 */
	public function mapDataToForms($imagePath, \Traversable $forms): void
	{
		// there is no data yet, so nothing to prepopulate
		if (null === $imagePath)
			return;

		/** @var FormInterface[] $forms */
		$forms = iterator_to_array($forms);

		// Get options
		$config = $forms['base64']->getParent()->getConfig();
		$uploadPath = trim($config->getOption('upload_path', " \t\n\r\0\x0B/\\"));

		// Get file path
		$dir = $this->kernel->getProjectDir().'/'.$uploadPath.'/';
		$filename = $dir . $imagePath;
		if(!is_file($filename))
			return;

		// To base64
		$type = pathinfo($filename, PATHINFO_EXTENSION);
		$data = file_get_contents($filename);
		$forms['base64']->setData(
			'data:image/' . $type . ';base64,' . base64_encode($data)
		);
	}

	public function mapFormsToData(\Traversable $forms, &$viewData): void
	{
		$forms = iterator_to_array($forms);
		$viewData = $forms['base64'] instanceof Form
			? $forms['base64']->getData()
			: null;
	}
}
