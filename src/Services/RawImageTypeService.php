<?php

namespace LoicPennamen\RawImageTypeBundle\Services;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class RawImageTypeService
{
	private $kernel;

	public function __construct(KernelInterface $kernel)
	{
		$this->kernel = $kernel;
	}

	/**
	 * @param Form $form
	 * @return string|null
	 * @throws \Exception
	 */
	public function makeFile(FormInterface $form, $returnFullPath = false)
	{
		$base64Meta = explode(',', trim($form->getData()));
		$base64Str = array_pop($base64Meta);

		$fullUploadPath = $this->checkUploadPath(
			$this->kernel->getProjectDir().'/'
			.trim($form->getConfig()->getOption('upload_path'), " \t\n\r\0\x0B/\\")
		);
		$filename = $form->getConfig()->getOption('filename');
		$shortname = $form->getConfig()->getOption('shortname');
		$deleteIfEmpty = $form->getConfig()->getOption('delete_if_empty');

		// Delete if empty?
		if(true === $deleteIfEmpty && !$base64Str && $filename) {
			if(is_file($fullUploadPath.'/'.$filename))
				unlink($fullUploadPath.'/'.$filename);
		}

		// No image
		if(!$base64Str)
			return null;

		// Sanitize and get binary data
		$binaryStr = $this->sanitizeBase64Image($base64Str);

		// Generate name
		if(!$filename) {
			$f = finfo_open();
			$extArr = explode('/', finfo_buffer($f, $binaryStr, FILEINFO_EXTENSION));
			$ext = $extArr[0];
			if($shortname)
				$filename = $shortname.'.'.$ext;
			else
				$filename = uniqid(mt_rand()).'.'.$ext;
		}

		// Generate path
		if(!file_exists($fullUploadPath))
			mkdir($fullUploadPath, 0755, true);

		// Copy file
		file_put_contents($fullUploadPath.'/'.$filename, $binaryStr);

		// Return file name
		if($returnFullPath)
			return $fullUploadPath.'/'.$filename;

		return $filename;
	}

	/**
	 * @param $fullUploadPath
	 * @throws \Exception
	 */
	private function checkUploadPath($fullUploadPath)
	{
		if(false !== strpos($fullUploadPath, '..'))
			throw new \Exception('Upload path can not contain the following string: ".."');

		return $fullUploadPath;
	}

	/**
	 * @param $base64Str
	 * @return false|string
	 * @throws \Exception
	 */
	private function sanitizeBase64Image($base64Str)
	{
		// Decode to binary
		$binaryStr = base64_decode($base64Str, true);
		if(false === $binaryStr)
			throw new \Exception('Invalid base64 data');

		// "image" mime type from binary"
		$f = finfo_open();
		$mimeType = finfo_buffer($f, $binaryStr, FILEINFO_MIME_TYPE);
		if('image' !== substr($mimeType, 0, 5))
			throw new \Exception('Posted base64 file is not an image');

		return $binaryStr;
	}
}
