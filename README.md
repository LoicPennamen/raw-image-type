# Symfony RawImage Type #
Symfony FormType for lighter image upload.  

jQuery must be available for the bundle to work.

This bundle uses the FontAwesome library:
[https://fontawesome.com/license](https://fontawesome.com/license)

## Installation

### Composer

Make sure Composer is installed globally, as explained in the
[installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

Open a command console, enter your project directory and execute:

```console
$ composer require loicpennamen/raw-image-type
```

If you are not using Symfony Flex, you need to enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
// config/bundles.php
return [
    // ...
    LoicPennamen\RawImageTypeBundle\RawImageTypeBundle::class => ['all' => true],
];
```

### Adding form_theme

In order to display the form, you must configure twig to use the bundle's template as such:

```yaml
# config/packages/twig.yaml
twig:
  paths:
    '%kernel.project_dir%/vendor/loicpennamen/raw-image-type/templates': RawImageTypeBundle
  form_themes:
    - "@RawImageTypeBundle\\raw_image_type.html.twig"
```

### Import assets

Add the file ``raw-image-type.css`` to your application styles.

```sass
// app.scss
@import "../../vendor/loicpennamen/raw-image-type/css/raw-image-type.css";
```

Add the file ``raw-image-type.js`` to your application scripts.

```typescript
// app.js
import $ from "jquery";
require('../../vendor/loicpennamen/raw-image-type/js/raw-image-type.js')($);
```




## How to use

### In your form

```php
namespace App\Form;

use LoicPennamen\RawImageTypeBundle\Form\RawImageType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class SiteConfigType extends AbstractType
{
  public function buildForm(FormBuilderInterface $builder, array $options)
  {
    $builder
      ->add('rawImage', RawImageType::class, [
         // Allowed file extensions
         'allowed_extensions' => ['.jpg', '.jpeg', '.png', '.gif', '.bmp'],
         // Delete existing file when filename option is specified and no data was posted
         'delete_if_empty' => true,
         // Display additional informations
         'debug' => false,
         // Text to display if no image was submitted
         'empty_text' => 'No image',
         // Desired file name after upload. See "shortname". Set to NULL or 'auto' to auto-generate a unique name
         'filename' => null,
         // Maximum width and height of the optimized posted image
         'max_width' => 1920,
         'max_height' => 1080,
         // Maximum width of the preview window, as CSS string
         'preview_max_width' => '100%',
         // Preview ratio - can be an arithmetic string
         'ratio' => '16/9',
         // Delete button title
         'remove_text' => 'Delete this image',
         // Required or not
         'required' => false,
         // Desired file name after upload - without the extension.
         'shortname' => null,
         // Where the actual file is stored
         'upload_path' => 'public/uploads/',
         
         // If your form is managing mapped Doctrine entities:
         'mapped' => false, 
      ])
      ;
  }
}
```

### Customize rendering

You can customize the visual aspect of the bundle via CSS classes prefixes with ``raw-image-type-``:

- raw-image-type-preview-wrapper
- raw-image-type-preview
- raw-image-type-preview-message
- raw-image-type-clear-wrapper


## How to use with a persisted entity

When submitted, the form entry ``rawImage`` will contain a base64 image. 
If you want to persist a path to your file in your database, 
you can consider using a different field name in your entity 
to hold this value as a string:

```php
// src/Entity/MyEntity

namespace App\Entity;

// ...

/**
 * @ORM\Entity(repositoryClass=MyEntityRepository::class)
 */
class MyEntity
{
    /**
    * This field will contain the path to the file on the server 
    * @ORM\Column(type="string", length=255, nullable=true)
    */
    private $image;
}
```

Then on form submit, use the tools vailable inside 
``LoicPennamen\RawImageTypeBundle\Services\RawImageTypeService`` 
to create and sanitize a "physical" file on your server.

**More documentation to come: This bundle is still a work in progress**
